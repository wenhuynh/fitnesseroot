!3 INITIALIZATION CODE
![~
!define TEST_SYSTEM {slim} 																				~ Uses FitNesse SliM to run tests
!path C:\AutomatedTesting\**.jar 																		~ Defines classpath for fixtures
| script | com.appiancorp.ps.automatedtest.fixture.TempoFixture | 										~ Initializes fixture to be used for the rest of the test
| setup selenium web driver with | BROWSER_NAME | browser | 											~ Initializes Selenium Web Driver and opens browser
]!

!3 FITNESSE KEYWORDS
![:
| script | 																								: This key word must be the first row of every script table 
| check | 																								: If the word check is in the first cell of a row, then a function call follows it. The last cell of the table is the expression we expect to be matched by what the function actually returns. 
| check not | 																							: If the word check not is in the first cell of a row, then a function call follows it. The last cell of the table is the expression we expect not to be matched by what the function actually returns.
| ensure | 																								: If the word ensure is in the first cell, then it should be followed by a function that should return a boolean true for green and false for red.
| reject | 																								: If the word reject is in the first cell, then it should be followed by a function that should return a boolean false for green and true for red.
| note |																								: If the word note is in the first cell, all other cells in that row will be ignored. Also works if the first cell is blank or begins with # or *.
| show | 																								: If the word show is in the first cell, then it should be followed by a function. A new cell will be added when the test is run, and it will contain the return value of the function.
| $VAR= | 																								: If a symbol assignment is in the first cell, then it should be followed by a function. The symbol is assigned the value returned by that function.
]!

!3 NAVIGATION METHODS
![:
| click on menu | TEMPO_MENU_NAME | 																	: Navigates to tempo tab
| search for | SEARCH_TERM |																			: Searches for a term on News, Reports, or Record List
]!

!3 NEWS METHODS
![:
| verify news feed containing text | NEWS_TEXT | is present | 											: Verify a news item with a particular title is present 
| verify news feed containing text | NEWS_TEXT | is not present | 										: Verify a news item with a particular title is not present
| toggle more info for news feed containing text | NEWS_TEXT | 											: Click the more info link 
| verify news feed containing text | NEWS_TEXT | and more info with label | LABEL | and value | VALUE | is present | : Verify the more info link of a particular new item contains a particular label and value
| verify news feed containing text | NEWS_TEXT | tagged with | TAG_NAME | is present | 					: Verify a news item with a particular title and tag is present 
| verify news feed containing text | NEWS_TEXT | commented with | COMMENT | is present | 				: Verify a news item with a particular title and comment is present 
| click on news feed | NEWS_TEXT | record tag | RECORD_TAG | 											: Click on record tag on a particular news item
| $VARIABLE_NAME= | get regex | REGEX | group | GROUP | from news feed containing text | NEWS_TEXT | 	: Use Java Regular Expression to return data from news as a variable
| $VARIABLE_NAME= | get regex | REGEX | group | GROUP | from news feed containing text | NEWS_TEXT | commented with | COMMENT |	: Use Java Regular Expression to return data from news as a variable
]!

!3 TASKS METHODS
![:
| click on task | TASK_NAME | 																			: Click on task
| click on task report | TASK_REPORT_NAME | 															: Click on task report
| verify task | TASK_NAME | is present | 																: Verify task is present
| verify task | TASK_NAME | is not present | 															: Verify task is not present
| verify task | TASK_NAME | has a deadline of | DEADLINE_TEXT | 										: Verify task has a particular deadline, e.g. 1h
]!

!3 RECORDS METHODS
![:
| click on record type | RECORD_TYPE_NAME | 															: Click on a record type
| click on record type user filter | USER_FILTER_NAME | 												: Click on a record type user filter
| verify record type user filter | USER_FILTER_NAME | is present | 										: Verify a particular record type user filter is present
| click on record | RECORD_NAME | 																		: Click on a record, works for both record list view and record grid view
| verify record | RECORD_NAME | is present | 															: Verify a record is present
| verify record | RECORD_NAME | is not present | 														: Verify a record is not present
| click on record view | VIEW_NAME | 																	: Click on a record view
| click on record related action | RELATED_ACTION_NAME | 												: Click on a related action, this works for quick links and on the related action view
| verify record related action | RELATED_ACTION_NAME | is present | 									: Verify a record related action is present
| verify record related action | RELATED_ACTION_NAME | is not present |									: Verify a record related action is not present
| sort record grid view by column | COLUMN_NAME | 														: Sort a record grid view by column
| click on record grid navigation | NAVIGATION_OPTION | 												: Page through a record grid view, options include "First", "Previous", "Next", or "Last"
]!

!3 REPORTS METHODS
![:
| click on report | REPORT_NAME | 																		: Click on a report
| verify report | REPORT_NAME | is present | 															: Verify a report is present
| verify report | REPORT_NAME | is not present |														: Verify a report is not present
]!

!3 ACTIONS METHODS
![:
| click on action | ACTION_NAME | 																		: Click on an action
| verify action | ACTION_NAME | is present | 															: Verify an action is present
| verify action | ACTION_NAME | is not present | 														: Verify an action is not present
]!

!3 INTERFACE METHODS
![:
| populate field | FIELD_LABEL_OR_INDEX | with | VALUE(S) | 											: When populating a picker field with a username, make sure to use the display value instead of the username
| populate field | FIELD_LABEL[FIELD_INDEX] | with | VALUE(S) | 										: Use this to populate the 2nd, 3rd, ... field with the same label
| populate | FIELD_TYPE | field | [FIELD_INDEX] | with | VALUE(S) | 									: Use this to populate the 1st, 2nd, 3rd, ... field of a specific type with no label, currently only compatible with FIELD_TYPEs of TEXT, PARAGRAH, and FILE_UPLOAD. WARNING: Integer, Decimal, and Encrypted text fields will be considered as Text fields.
| populate field | FIELD_LABEL | with value | VALUE | 													: Use this to populate a field with a value that contains a comma
| populate field |[FIELD_INDEX] | in section | SECTION_NAME | with | VALUE(S) |							: Populate a field in a section with no label<br/>
| clear field | FIELD_LABEL | of | VALUE_TO_REMOVE | 													: Use this to remove a specific value from a picker
| get form title | 																						: Use with FitNesse keyword 'check' to verify title
| get form instructions | 																				: Use with FitNesse keyword 'check' to verify title
| get field | FIELD_LABEL_OR_INDEX | value | 															: Use with FitNesse key words 'check'  to verify value runtime variables
| get field | FIELD_LABEL_OR_INDEX | in section | SECTION_NAME | value | 								: Use with FitNesse key words 'check' to verify values runtime variables
| verify field | FIELD_LABEL_OR_INDEX | contains | VALUE | 												: Verify field contains value
| verify field | FIELD_LABEL | contains value | VALUE | 												: Use this to verify a field that contains a comma
| verify field | FIELD_LABEL_OR_INDEX | in section | SECTION_NAME | contains | VALUE | 					: Verify field in section contains a value
| verify field | FIELD_LABEL_OR_INDEX | is present | 													: Verify field with label is present
| verify field | FIELD_LABEL_OR_INDEX | is not present | 												: Verify field with label is not present
| verify field | FIELD_LABEL_OR_INDEX | contains validation message | VALIDATION_MESSAGE(S) |			: Verify field with label contains a specific validation message
| verify section | SECTION_NAME | contains validation message | VALIDATION_MESSAGE(S) |					: Verify section with label contains a specific validation message
| populate grid | GRID_NAME | column | COLUMN_NAME_OR_NUMBER | row | ROW_NUMBER | with | VALUE(S) | 	: Populate a grid field with a value
| verify grid | GRID_NAME | column | COLUMN_NAME_OR_NUMBER | row | ROW_NUMBER | contains | VALUE(S) | 	: Verify a grid field contains a value
| select grid | GRID_NAME | row | ROW_NUMBER | 															: Selects row in grid
| verify grid | GRID_NAME | row | ROW_NUMBER | is selected | 											: Verifies if row in grid is selected
| click on grid | GRID_NAME | add row link | 															: Can use gridName, gridIndex, or gridName[index] 
| click on grid | GRID_NAME_OR_INDEX | navigation | NAV_REFERENCE | 									: Navigates through paging, only takes "first", previous, next, or "last"
| sort grid | GRID_NAME_OR_INDEX | by | COLUMN_NAME | 													: Sorts grid by column name
| click on link | LINK_NAME | 																			: Can use linkName or linkName[index]
| click on button | BUTTON_NAME | 																		: Clicks on button
| verify button | LABEL | is present |																	: Verify button with label is present
| verify button | LABEL | is not present |																: Verify button with label is not present
| click on radio option | RADIO_OPTION | 																: Clicks on the first radio button with the given choice label
| click on radio option | RADIO_OPTION[INDEX] | 														: Clicks on the radio button with the given choice label by index. Index only includes radio button options.
| click on checkbox option | CHECKBOX_OPTION | 															: Checks on the first checkbox with the given choice label
| click on checkbox option | CHECKBOX_OPTION[INDEX] | 													: Checks on the checkbox with the given choice label by index. Index only includes checkbox options.
| expand section | SECTION_NAME | 																		: Expands section 
| collapse section | SECTION_NAME |																		: Collapses section
]!

!3 UTILITY METHODS
![:
| setup selenium web driver with | BROWSER_NAME | browser | 											: Initializes Selenium Web Driver and opens browser, options include FIREFOX, CHROME, IE
| set appian url to | APPIAN_URL | 																		: Sets the default appian url
| set appian version to | APPIAN_VERSION |																: Sets the default appian version
| set appian locale to | APPIAN_LOCALE |																: Sets the Appian locale used for date and datetime formatting, options include en_US or en_GB (defaults to en_US)
| set start datetime | 																					: Used as relative datetime for date/time fields
| set timeout seconds to | TIMEOUT_SECONDS | 															: Used as the wait timeout in between each command
| set screenshot path to | SCREENSHOT_PATH | 															: Set the folder where screenshots will be saved. This will create new folders if necessary. Terminate path with "/" to ensure folder creation. (Defaults to AUTOMATED_TESTING_HOME\screenshots\)
| set take error screenshots to | SCREENSHOT_BOOLEAN | 													: If true, screenshots will be taken for every failed test case. (Default true)
| set stop on error to | STOP_ON_ERROR_BOOLEAN | 														: If true, the FitNesse test will stop on any error. (Default false)
| take screenshot | FILE_NAME | 																		: Take a screenshot with the name (do not include the extension)
| login with username | USERNAME | and password | PASSWORD | 											: Uses the previously set appian url
| login with terms with username | USERNAME | and password | PASSWORD | 								: Uses set appian url
| wait for | RELATIVE_PERIOD, e.g. +1 days, +1 hours | 													: Waits for relative amount of time
| wait until | RELATIVE_PERIOD | 																		: Waits until relative time
| $VARIABLE_NAME= | get random string | STRING_LENGTH | 												: Use $VARIABLE_NAME to access variable
| $VARIABLE_NAME= | get random integer from | MIN_INT | to | MAX_INT | 									: Use $VARIABLE_NAME to access variable
| $VARIABLE_NAME= | get random decimal from | MIN_DECIMAL | to | MAX_DECIMAL | 							: Use $VARIABLE_NAME to access variable
| $VARIABLE_NAME= | get random decimal from | MIN_DECIMAL | to | MAX_DECIMAL | with | DECIMAL_PLACES | 	: Use $VARIABLE_NAME to access variable
| refresh | 																							: refreshes screen
| logout | 																								: Logout of Appian
]!
