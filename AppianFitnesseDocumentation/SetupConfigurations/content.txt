!1 Appian Setup Configurations

!3 These variables are required for every test script that you write:
 * '''variable defined: TEST_SYSTEM=slim''' 
 * '''classpath: C:\AutomatedTesting\**.jar'''
 * '''!| script| com.appiancorp.ps.automatedtest.fixture.TempoFixture|'''
 * '''|setup selenium web driver with browser|BROWSER| '''
  * Replace with FIREFOX, IE or CHROME
 * '''|set appian url to |APPIAN_URL| '''
  * Replace APPIAN_URL
 * '''|login with username|USERNAME|and password|PASSWORD|'''
  * Replace USERNAME and PASSWORD
 * '''|login with terms with username|USERNAME|and password|PASSWORD|'''
  * Use this for Appian instances with terms

!3 These variables are optional for every test script that you write:
 * '''|set appian version to  |VERSION_NUMBER |'''
  * Currently supporting versions 7.10, 7.11, 16.1 
 * '''|set appian locale to |LOCALE|'''
  * en_US or en_GB
 * '''|set stop on error to  |TRUE_OR_FALSE |'''
  * Stops test if there is an error
 * '''|set timeout seconds to |TIME|'''
  * Amount of time to wait for an element to appear on screen
 * '''|set start datetime |'''
  * Uses the Start date  and time when a test starts and can be referenced to 
