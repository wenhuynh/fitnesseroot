!*** Create New Package
|script|
| $SPArand1= | get random integer from | 111 | to | 999 |
| $rand= | get random string | 6 |
| Create New Package |

|script|
| Create Blank New Health Homes Program with programname | New Program $rand |

|script|
| Summary Official After Program Creation with spa ID|${STATE}-16-$SPArand1|with Key Contact lastname |${KEYLAST}| with firstname |${KEYFIRST}|

|script|
| Public Comment None | 

|script|
| Tribal Input None | 

|script|
| SAMHSA | 

|script|
| Intro | 

|script|
| Population and Enrollment Two Conditions| 

|script|
| Geographic Limitations Statewide| 

|script|
| Services with file uploaded from location|${FILELOCATION}|

|script|
| Providers - Designated Physicians and Rural Health Clinics| 

|script|
| Service Delivery Systems - Fee for Service | 

|script|
| Payment Methodologies - PCCM| 

|script|
| Monitoring| 

|script|
| Get Package ID|
***!

!*** Go to records to submit task to SPOC (Don't forget to change the state code inside to your state!)
|script|
|Editor Forwards Submission Package to SPOC with package ID|$PACKAGEID|with state code|${STATE}|
***!

!*** tasks: SPOC to Director back to SPOC to CPOC
|script|
|Logout and Login to Review Submission Package Task with Accept with username|${STATEPOC}|with password|Appian123|with package ID|$PACKAGEID|with choice|Forward Submission Package to Director|

|script|
|Logout and Login to Review Submission Package Task with Accept with username|${STATEDIRECTOR}|with password|Appian123|with package ID|$PACKAGEID|with choice|Certify and Forward Submission Package to State POC for Submission to CMS|

|script|
|Logout and Login to Review Submission Package Task with username|${STATEPOC}|with password|Appian123|with package ID| $PACKAGEID|with choice|Submit the Submission Package to CMS|
***!

!*** Assign Reviewable Units
|script|
|Assign Reviewable Units with CPOC username|${CPOC}|with password|Appian123|with package ID|$PACKAGEID|with milestone date|+30 days|with SRT1|${SRT1}|with SRT2|${SRT2}|with SRT3|${SRT3}|

|script|
|Open Review Submission Package Task with username|${CPOC}|with password|Appian123|with package ID|$PACKAGEID|

|script|
|Mark Review Tool Section with sectionname|Submission - Summary|with review assessment collapsible number|13|mark as review assessment|Sufficient|

|script|
|Mark Review Tool Section with sectionname|Submission - Medicaid State Plan|with review assessment collapsible number|2|mark as review assessment|Sufficient|

|script|
|Mark Review Tool Section with sectionname|Submission - Public Comment|with review assessment collapsible number|2|mark as review assessment|Sufficient|

|script|
|Mark Review Tool Section with sectionname|Submission - Tribal Input|with review assessment collapsible number|2|mark as review assessment|Sufficient|

|script|
|Mark Review Tool Section with sectionname|Submission - SAMHSA Consultation|with review assessment collapsible number|2|mark as review assessment|Sufficient|

|script|
|Mark Review Tool Section with sectionname|Health Homes Intro|with review assessment collapsible number|5|mark as review assessment|Sufficient|

|script|
|Mark Review Tool Section with sectionname|Health Homes Population and Enrollment Criteria|with review assessment collapsible number|5|mark as review assessment|Sufficient|

|script|
|Mark Review Tool Section with sectionname|Health Homes Geographic Limitations|with review assessment collapsible number|2|mark as review assessment|Sufficient|

|script|
|Mark Review Tool Section with sectionname|Health Homes Services|with review assessment collapsible number|4|mark as review assessment|Sufficient|

|script|
|Mark Review Tool Section with sectionname|Health Homes Providers|with review assessment collapsible number|6|mark as review assessment|Sufficient|

|script|
|Mark Review Tool Section with sectionname|Health Homes Service Delivery Systems|with review assessment collapsible number|2|mark as review assessment|Sufficient|

|script|
|Mark Review Tool Section with sectionname|Health Homes Payment Methodologies|with review assessment collapsible number|4|mark as review assessment|Sufficient|

|script|
|Mark Review Tool Section with sectionname|Health Homes Monitoring|with review assessment collapsible number|4|mark as review assessment|Sufficient|
|note|------------numbers of collapsible will be different in summary SF if draft package! As will choice name!---------------------|
|note|------------numbers of collapsible will be different in Payment Methodology will be different depending on if Fee for Service is checked (adds 2 more +/- fields)!---------------------|
|note|with review assessment collapsible number = number of "+/-" links there are until one reaches the +/- link that opens the review assessment section (until and including)|

|script|
|Take Action on package with choice|Request Additional Information regarding the Submission Package|

***!


!*** RAI
|script|
|CPOC RAI Questions with username|${CPOC}|with password|Appian123|with package ID|$PACKAGEID|
***!

!contents -R2 -g -p -f -h
