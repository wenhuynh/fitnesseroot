!3 Update the Appian URL (REPLACE_APPIAN_URL), Username (REPLACE_USERNAME), and Password (REPLACE_PASSWORD) below.
!3 Update the Appian Locale (en_US or en_GB) and Appian Version (7.10, 7.11, or 16.1) if different. 


!*** Setup
Need these before each test case
!| script | com.appiancorp.ps.automatedtest.fixture.TempoFixture |
| setup selenium web driver with browser | FIREFOX |
| set appian url to | https://gmacbisdev.appiancloud.com/suite/tempo/ |

Setup how long test will wait before failing 
| script |
| set timeout seconds to | 20 |

Set start date and format how dates are used in the application
| script |
| set start datetime |
| set appian version to | 7.11 |
| set appian locale to | en_US |
| set stop on error to | false |

Create a random string and new folder to take screensshots for errors
| script |
| $rand= | get random string | 4 |
| set take error screenshots to | true |
| set screenshot path to | C:\AutomatedTesting\screenshots\$rand\ |
***!

!*** Update Variables/Initial Login information
| script | 
| login with username |	HIStateEditorMHHP | and password | Appian123 |

!define STATE {HI}

!define KEYLAST {Smith}

!define KEYFIRST {Robert}

!define STATEPOC {HIStatePOCMHHP}

!define STATEDIRECTOR {HIStateDirectorMHHP}

!define CPOC {CMSPOCMHHP9}

!define FILELOCATION {C:\AutomatedTesting\FitNesseRoot\files\blank.docx}

!define AMENDPROGRAM {New Program Ne61EF}

!define COUNTIES {Honolulu}

!define SRT1 {SRT 1 MHHP CMS}

!define SRT2 {SRT 2 MHHP CMS}

!define SRT3 {SRT 3 MHHP CMS}

!define FIRSTSRMANAGER {CMSSRMGRM1}

!define PACKAGEAPPROVER {CMSPAM2}

!define CMSPOCADMINM {CMSPOCADMINM1}

!define DISAPPROVER {CMSPDISAPRVRM1}

!define OSORA {CMSOSORAM1}
*!